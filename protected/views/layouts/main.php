<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />



	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
<table width="100%"><tr><td>
	<div id="header">
		<div id="logo"><a href="<?php echo Yii::app()->baseUrl ?>" class="thumbnail" ><img src="<?php echo Yii::app()->baseUrl ?>/images/Dreamads%20Logo.jpg" width="250px" alt="" /></a></div>
	</div><!-- header -->
    </td><td align="right">
	<div id="mainmenu">

		<?php
        $this->widget('bootstrap.widgets.TbTabs', array(
            'type'=>'tabs',
            'tabs'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'About Us', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Services','url'=>array('/site/services')),
                array('label'=>'Contact', 'url'=>array('/site/contact'))
                ),
        ));

        ?>
	</div><!-- mainmenu --></td></tr></table>


	<?php echo $content; ?>

	<div class="clear"></div>





</div><!-- page -->
<div  style="position: absolute; left:35%; ">
    <small><p >
                    Copyright &copy; <?php echo date('Y'); ?> by DreamAds.</p>
           <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All Rights Reserved.</p>
           </small>
</div>
</body>
</html>
