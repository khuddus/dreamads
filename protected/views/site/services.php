<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Services';

?>

<h1>Services</h1>
<table>
    <tr>
        <td>
<div id="yw0">
    <ul id="yw1" class="nav nav-pills nav-stacked">
        <li class="active">
            <a data-toggle="tab" href="#tv_ad_making">Tv Ad Making</a>
        </li>
        <li>
            <a data-toggle="tab" href="#theatre_ads">Theatre ads</a>
        </li>
         <li>
            <a data-toggle="tab" href="#corporate_videos">Corporate Videos</a>
        </li>
       <!--  
         <li>
            <a data-toggle="tab" href="#short_films">ShortFilms</a>
        </li>
          --> 
      <li>
            <a data-toggle="tab" href="#documentaries">Documentaries</a>
        </li>
       <!--  
        <li>
            <a data-toggle="tab" href="#presentation">Presentations</a>
        </li>
            --> 
      <li>
            <a data-toggle="tab" href="#voice_recording">Voice Recording</a>
        </li>
        <li>
            <a data-toggle="tab" href="#dubbing">Dubbing</a>
        </li>
        <li>
            <a data-toggle="tab" href="#titles">Titles</a>
        </li>
        <li>
            <a data-toggle="tab" href="#vfx">VFX</a>
        </li>
        <li>
            <a data-toggle="tab" href="#editing">Editing</a>
        </li>
        <!--  
      <li>
            <a data-toggle="tab" href="#video_recording">HD Video Coverage</a>
        </li>
        <li>
            <a data-toggle="tab" href="#crane">Crane &amp; Track Shooting</a>
        </li>
            --> 
      <li>
            <a data-toggle="tab" href="#concepts">Concepts</a>
        </li>
      <li>
            <a data-toggle="tab" href="#drone">Drone Camera Shooting</a>
        </li>
    </ul>
        </td><td>
    <div class="tab-content">
        <div  id="tv_ad_making" class="tab-pane fade active in" >
            <table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Telugu%20Channels.jpg" width="600px" height="600px" alt="Dream ads tv ads telivision advertisements" /></td>

                    <td>We make advertisements for television channels..</td>
                </tr>
            </table>
            </div>
        <!--  
        <div id="ad_films" class="tab-pane fade">
            <table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Ad%20Films.png" width="600px" height="600px" alt="Dream ads adfilms" /></td>

                    <td>We make adfilms in order to display in theatres</td>
                </tr>
            </table>
        </div>
        --> 
        <div id="theatre_ads" class="tab-pane fade">
            <table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/TheatreAds.jpg" width="600px" height="600px" alt="Dream ads Theatre ads" /></td>

                    <td>We make theatre ads</td>
                </tr>
            </table>
        </div>
         <div id="corporate_videos" class="tab-pane fade">
            <table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Corporatevideos.jpg" width="600px" height="600px" alt="Dream ads adfilms" /></td>

                    <td>We develop stupendous corporate videos</td>
                </tr>
            </table>
        </div>
       
       <!--  
         <div id="short_films" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Shorfilms-1.png" width="600px" height="600px" alt="Dream ads Short Films" /></td>

                    <td>We make short films and also provide all the equipments and technologies needed for short films</td>
                </tr>
            </table>
            </div>
   --> 
     
        <div id="documentaries" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Documentries.jpg" width="600px" height="600px" alt="Dream ads do Documentaries documentary " /></td>

                    <td>We provide Documentaries for all kinds of your needs..</td>
                </tr>
            </table></div>

       <!--  
       <div id="presentation" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Presentations.png" width="600px" height="600px" alt="Dream ads do presentations" /></td>

                    <td>We design presentation for all kinds of needs..</td>
                </tr>
            </table></div>
  --> 
        <div id="voice_recording" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/VoiceRecording.jpg" width="600px" height="600px" alt="Dream ads provide voice recording facilities" /></td>

                    <td>We provide voice recording facility for announcments or advertisements and all different kinds of needs</td>
                </tr>
            </table></div>

        <div id="dubbing" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Dubbing.jpg" width="600px" height="600px" alt="Dream ads provide voice dubbing" /></td>

                    <td>We have excellent dubbing team who will provide you the best quality output</td>
                </tr>
            </table></div>

        <div id="titles" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Titles.jpg" width="600px" height="600px" alt="Dream ads provide titles" /></td>



                    <td>We provide titles for all kinds of videos like shortfilms, marriages etc..</td>
                </tr>
            </table></div>
        <div id="vfx" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/VFX.jpg" width="600px" height="600px" alt="Dream ads do VFX" /></td>

                    <td>We provide VFX for your shortfilms or any kind of videos</td>
                </tr>
            </table></div>

        <div id="editing" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Editing.png" width="600px" height="600px" alt="Dream ads editing" /></td>

                    <td>We provide world-class proffessional editing for your films</td>
                </tr>
            </table></div>
 <div id="concepts" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/Concepts.jpg" width="600px" height="600px" alt="Dream ads editing" /></td>

                    <td>We write creative concepts for your advertising</td>
                </tr>
            </table></div>
 <div id="drone" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/DroneCameraShoot.jpg" width="600px" height="600px" alt="Dream ads editing" /></td>

                    <td>We Drone Cameras for your shooting purposes</td>
                </tr>
            </table></div>

       <!--  
        <div id="video_recording" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/HD%20Video.JPG" width="600px" height="600px" alt="dreamads HD video facilities" /></td>

                    <td>We provide HD video recording facilities and resources needed for you..</td>
                </tr>
            </table></div>
        <div id="crane" class="tab-pane fade"><table width="100%">
                <tr>
                    <td><img src="<?php echo Yii::app()->baseUrl ?>/images/jib.jpg" width="600px" height="600px" alt="dream ads track and crane shooting facilities" /></td>

                    <td>We provide crane and track facilities for shooting of films</td>
                </tr>
            </table></div>
              -->
    </div>
        </td>
    </tr>
</table>
</div>
</div>
